The repository contains 3 different directories for the 3 research questions

RQ1:
The file research groups per author.xq can be opened and run in BaseX. It counts the amount of different research groups each other has written a paper for. The result of the query can be found in the .xml file.

RQ2:
expert on topic.xq contains the query that can be run. For each author it returns the author name and the amount of papers that author wrote on a certain topic. Title, keyword and abstract are searched for the topic. "topic"  can be changed into any keyword you want to search for. Again, the result for the query is in the .xml files.

RQ3:
perdate_gerenal.xq looks for each timestamp how many papers of a certain topic are in the database. Title, keywords and abstract are searched to see if they contain this keyword. "topic" can be changed to any keyword you want to search for. Results for the query are in the .xml files.