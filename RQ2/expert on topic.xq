<root>{
for $entry in doc("eprints-2015-02-01.xml")/eprints
let $authorlist :=  $entry/paper/author
for $author in distinct-values($authorlist)
return

<result>
<author>{$author}</author>

<number>{count(
let $paperlist := $entry/paper
for $goodpaper in $paperlist
where $goodpaper/author[. eq $author]  

let $result :=  $goodpaper
where $goodpaper/abstract[. contains  text "topic"] or
      $goodpaper/title[. contains text "topic"] or
      $goodpaper/keywords[. contains text "topic"]

return ($result))
}
</number>

</result>
}
</root>
