<root>{
for $entry in doc("eprints-2015-02-01.xml.xml")/eprints
let $datelist :=  $entry/paper/datestamp
for $datestamp in distinct-values($datelist)
return

<result>
<datestamp>{$datestamp}</datestamp>

<number>{count(
let $paperlist := $entry/paper
for $datedpaper in $paperlist
where $datedpaper/datestamp[. eq $datestamp]  

let $result :=  $datedpaper
where $datedpaper/abstract[. contains  text "topic"] or
      $datedpaper/title[. contains text "topic"] or
      $datedpaper/keywords[. contains text "topic"]

return ($result))
}
</number>

</result>
}
</root>