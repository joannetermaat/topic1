<root>{
for $entry in doc("eprints-2015-02-01.xml")/eprints
let $authorlist :=  $entry/paper/author
for $author in distinct-values($authorlist)
return

<result>
<author>{$author}</author>

<groups>{count(distinct-values(
let $paperlist := $entry/paper
for $goodpaper in $paperlist
where $goodpaper/author[. eq $author]

let $grouplist := $goodpaper/research_groups
for $group in $grouplist
return ($group)))
}
</groups>

</result>
}
</root>
